## runtime enviroment

Please change to your directory and file name in `test.go`.

```golang
InsertBeforeSysPath("/Users/guanpengchn/Public/anaconda3/envs/modelchain/lib/python2.7/site-packages")
hello := ImportModule("/Users/guanpengchn/Code/test/go-python-test", "test")
```

run the demo

```bash
go run ./test.go
```

if `import keras` in hello.py, you will get an error such as:

```bash
panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0x1 addr=0x0 pc=0x4093a66]

goroutine 1 [running]:
github.com/sbinet/go-python.(*PyObject).Call(0x0, 0xc4200b0160, 0xc4200b00f8, 0x0)
        /Users/guanpengchn/go/src/github.com/sbinet/go-python/object.go:315 +0x26
main.main()
        /Users/guanpengchn/Code/test/go-python-test/test.go:36 +0x2d7
exit status 2
```